package gov.app.intarujakartabarat.data.sources

class RdrtDataSource : ZntDataSource() {
  override fun sourceLabel(): String = "RDRT"
  override fun endPoint(): String = "https://syc.smartptsl.com/service-rdrt.php"
}