package gov.app.intarujakartabarat.data

import android.graphics.Color
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Polygon
import com.google.android.gms.maps.model.PolygonOptions
import gov.app.intarujakartabarat.data.pojo.Shapes
import gov.app.intarujakartabarat.utils.Tm3Utils
import gov.app.intarujakartabarat.utils.TransformableCoordinate
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PolygonDrawer(
  private val googleMap: GoogleMap,
  val showAttrib: (Map<String, Any>) -> Unit
) : GoogleMap.OnPolygonClickListener {

  private val displayedPolygons = mutableListOf<DisplayedPolygon>()
  var previousClickedPolygon: Polygon? = null

  init {
    googleMap.setOnPolygonClickListener(this)
  }

  override fun onPolygonClick(clickedPolygon: Polygon?) {
    val attribute = displayedPolygons.firstOrNull { it.polygon == clickedPolygon }
    attribute?.attributes?.let { showAttrib(it) }
    previousClickedPolygon?.strokeWidth = 1f
    clickedPolygon?.strokeWidth = 5f
    previousClickedPolygon = clickedPolygon
  }

  fun draw(data: List<Shapes>, zone: Tm3Utils.Zone, notifyProcess: (Int) -> Unit) {
    displayedPolygons.forEach { it.polygon.remove() }
    displayedPolygons.clear()
    var processed = 0
    Observable.fromIterable(data)
      .subscribeOn(Schedulers.newThread())
      .forEach { shape ->
        Observable.fromIterable(shape.polygon)
          .subscribeOn(Schedulers.newThread())
          .observeOn(Schedulers.computation())
          .map { list ->
            TransformableCoordinate(zone, Pair(list[0], list[1])).getTransformedCoordinate()
          }.toList()
          .subscribeOn(Schedulers.newThread())
          .map { points ->
            DataPolygon(shape.attribut, points, shape.fill, shape.stroke)
          }
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe { dataPolygon ->
            notifyProcess(++processed)
            displayedPolygons.add(createPolygon(dataPolygon))
          }
      }.isDisposed
  }

  private fun createPolygon(dataPolygon: DataPolygon): DisplayedPolygon {
    val polygon = googleMap.addPolygon(
      PolygonOptions()
        .addAll(dataPolygon.polygonPoints)
        .clickable(true)
        .strokeColor(Color.parseColor(dataPolygon.strokeColor))
        .fillColor(Color.parseColor(dataPolygon.fillColor))
        .strokeWidth(1f)
    )
    return DisplayedPolygon(dataPolygon.attributes, polygon)
  }

}

data class DisplayedPolygon(val attributes: Map<String, Any>, val polygon: Polygon)
data class DataPolygon(
  val attributes: Map<String, Any>,
  val polygonPoints: List<LatLng>,
  val fillColor: String,
  val strokeColor: String
)