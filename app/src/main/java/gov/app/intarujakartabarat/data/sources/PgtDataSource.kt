package gov.app.intarujakartabarat.data.sources

class PgtDataSource : ZntDataSource() {
  override fun endPoint(): String = "http://syc.smartptsl.com/service-pgt.php"
  override fun sourceLabel(): String = "PGT"
}