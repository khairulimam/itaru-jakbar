package gov.app.intarujakartabarat.data.sources

import com.loopj.android.http.RequestParams
import gov.app.intarujakartabarat.data.filter.LocationCriteria

abstract class HttpDataSource {
  abstract fun endPoint(): String
  abstract fun params(criteria: LocationCriteria, radius: Double): RequestParams
  abstract fun sourceLabel(): String
}