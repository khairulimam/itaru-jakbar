package gov.app.intarujakartabarat.data.sources

import android.util.Log
import com.loopj.android.http.RequestParams
import gov.app.intarujakartabarat.data.filter.LocationCriteria
import gov.app.intarujakartabarat.utils.toTm3

class BtDataSource : HttpDataSource() {

  override fun sourceLabel(): String = "BT"
  override fun endPoint(): String = "https://syc.smartptsl.com/service.php"

  override fun params(criteria: LocationCriteria, radius: Double): RequestParams {
    val southWest = criteria.getSouthWest(radius).toTm3()
    val northEast = criteria.getNorthEast(radius).toTm3()
    return RequestParams().apply {
      put("x1", southWest.first)
      put("x2", northEast.first)
      put("y1", southWest.second)
      put("y2", northEast.second)
    }
  }
}