package gov.app.intarujakartabarat.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import gov.app.intarujakartabarat.R
import gov.app.intarujakartabarat.requests.Http
import kotlinx.android.synthetic.main.login.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.okButton
import org.jetbrains.anko.startActivity

class Login : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.login)
    btnLogin.setOnClickListener {
      val loading = indeterminateProgressDialog("Mencoba login", "Mohon tunggu")
      loading.show()
      login({
        loading.dismiss()
        linearLayout.snackbar("Login berhasil")
        appPreference().edit().apply {
          putBoolean(Splash.LOGGED_IN, true)
        }.apply()
        startActivity<MapsActivity>()
      }, {
        loading.dismiss()
        alert(
          getString(R.string.login_error),
          "Mohon Maaf"
        ) {
          okButton { }
        }.show()
      })
    }

    btnCreateAccount.setOnClickListener {
      startActivity<Register>()
      finish()
    }

    btnForgetAccount.setOnClickListener {
      startActivity<Reset>()
      finish()
    }
  }

  private fun login(success: () -> Unit, failed: () -> Unit) {
    Http.client.get("https://syc.smartptsl.com/login.php", RequestParams(
      mapOf(
        "username" to etEmail.text.toString(),
        "password" to etPassword.text.toString()
      )
    ), object : AsyncHttpResponseHandler() {
      override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
        success()
      }

      override fun onFailure(
        statusCode: Int,
        headers: Array<out Header>?,
        responseBody: ByteArray?,
        error: Throwable?
      ) {
        failed()

      }

    })
  }
}