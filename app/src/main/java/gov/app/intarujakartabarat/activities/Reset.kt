package gov.app.intarujakartabarat.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns
import android.widget.EditText
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import gov.app.intarujakartabarat.R
import gov.app.intarujakartabarat.requests.Http
import kotlinx.android.synthetic.main.reset.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.okButton
import org.jetbrains.anko.startActivity

class Reset : AppCompatActivity() {

    companion object {

        private const val EMAIL = "EMAIL"
        private const val PASSWORD = "PASSWORD"
        private const val RE_PASSWORD = "RE_PASSWORD"
    }

    private var forms: MutableMap<String, EditText>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.reset)
        forms = mutableMapOf(
            EMAIL to etPassword,
            PASSWORD to etRePassword,
            RE_PASSWORD to etRePassword
        )

        btnLogin.setOnClickListener {
            forms?.forEach {
                if (it.value.text.trim().isEmpty()) {
                    it.value.error = "Inputan tidak boleh kosong!"
                    return@setOnClickListener
                }
            }
            if (!etPassword.text.toString().equals(etRePassword.text.toString(), true)) {
                etPassword.error = "Password harus sama!"
                etRePassword.error = "Password harus sama!"
                return@setOnClickListener
            }
            if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.text).matches()) {
                etEmail.error = "Email tidak valid. Silahkan masuk email yang valid!"
                return@setOnClickListener
            }
            val loading = indeterminateProgressDialog("Sedang proses reset password")
            loading.show()
            reset({
                loading.dismiss()
                linearLayout.snackbar("Proses  reset  telah berhasil dilakukan, silahkan cek email anda")
            }, {
                loading.dismiss()
                alert("Ada kesalahan. Mohon coba kembali", "Mohon Maaf") {
                    okButton { }
                }.show()
            })
        }
    }

    override fun onBackPressed() {
        startActivity<Login>()
        finish()
    }


    private fun reset(success: () -> Unit, failed: () -> Unit) {
        Http.client.get(
            "https://syc.smartptsl.com/reset.php",
            RequestParams(
                mapOf(
                    "email" to etEmail.text.toString(),
                    "password" to etPassword.text.toString()
                )
            ),
            object :
                AsyncHttpResponseHandler() {
                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                    success()
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Array<out Header>?,
                    responseBody: ByteArray?,
                    error: Throwable?
                ) {
                    failed()
                }
            })
    }


}
