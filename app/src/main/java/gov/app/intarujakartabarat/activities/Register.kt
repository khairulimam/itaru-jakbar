package gov.app.intarujakartabarat.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns.EMAIL_ADDRESS
import android.widget.EditText
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import gov.app.intarujakartabarat.R
import gov.app.intarujakartabarat.requests.Http
import kotlinx.android.synthetic.main.register.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.okButton
import org.jetbrains.anko.startActivity

class Register : AppCompatActivity() {

  companion object {
    private const val NAME = "NAME"
    private const val EMAIL = "EMAIL"
    private const val PASSWORD = "PASSWORD"
    private const val RE_PASSWORD = "RE_PASSWORD"
  }

  private var forms: MutableMap<String, EditText>? = null


  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.register)
    forms = mutableMapOf(
      NAME to etName,
      EMAIL to etEmail,
      PASSWORD to etPassword,
      RE_PASSWORD to etRePassword
    )
    btnRegister.setOnClickListener {
      forms?.forEach {
        if (it.value.text.trim().isEmpty()) {
          it.value.error = "Inputan tidak boleh kosong!"
          return@setOnClickListener
        }
      }
      if (!etPassword.text.toString().equals(etRePassword.text.toString(), true)) {
        etPassword.error = "Password harus sama!"
        etRePassword.error = "Password harus sama!"
        return@setOnClickListener
      }
      if (!EMAIL_ADDRESS.matcher(etEmail.text).matches()) {
        etEmail.error = "Email tidak valid. Silahkan masuk email yang valid!"
        return@setOnClickListener
      }
      val loading = indeterminateProgressDialog("Sedang membuat akun baru")
      loading.show()
      register({
        loading.dismiss()
        linearLayout.snackbar("Akun telah berhasil dibuat, silahkan login")
      }, {
        loading.dismiss()
        alert("Ada kesalahan. Mohon coba kembali", "Mohon Maaf") {
          okButton { }
        }.show()
      })
    }
  }

  private fun register(success: () -> Unit, failed: () -> Unit) {
    Http.client.get(
      "https://syc.smartptsl.com/register.php",
      RequestParams(
        mapOf(
          "email" to etEmail.text.toString(),
          "full_name" to etName.text.toString(),
          "password" to etPassword.text.toString()
        )
      ),
      object :
        AsyncHttpResponseHandler() {
        override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
          success()
        }

        override fun onFailure(
          statusCode: Int,
          headers: Array<out Header>?,
          responseBody: ByteArray?,
          error: Throwable?
        ) {
          failed()
        }
      })
  }

  override fun onBackPressed() {
    startActivity<Login>()
    finish()
  }

}

class OnTextChange(private val changed: () -> Unit) : TextWatcher {
  override fun afterTextChanged(s: Editable?) {}
  override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
  override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    changed()
  }
}