package gov.app.intarujakartabarat.activities

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import gov.app.intarujakartabarat.R
import org.jetbrains.anko.startActivity

class Splash : AppCompatActivity() {

  companion object {
    const val LOGGED_IN = "LOGGED_IN"
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_splash)
    Thread(Runnable {
      Thread.sleep(2000)
      val loggedIn = appPreference().getBoolean(LOGGED_IN, false)
      if (!loggedIn) startActivity<Login>()
      else startActivity<MapsActivity>()
      finish()
    }).start()
  }
}

fun Context.appPreference() = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE)