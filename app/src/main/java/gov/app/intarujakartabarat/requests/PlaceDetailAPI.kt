package gov.app.intarujakartabarat.requests

import android.content.Context
import com.google.android.gms.maps.model.LatLng
import com.loopj.android.http.JsonHttpResponseHandler
import cz.msebera.android.httpclient.Header
import gov.app.intarujakartabarat.R
import org.json.JSONObject

class PlaceDetailAPI(private val ctx: Context) {
  companion object {
    private const val DETAIL_ENDPOINT =
      "https://maps.googleapis.com/maps/api/place/details/json?placeid=%s&fields=name,geometry&key=%s"
  }

  fun getDetail(placeId: String, succedd: (LatLng) -> Unit) {
    Http.client.get(
      DETAIL_ENDPOINT.format(placeId, ctx.getString(R.string.NO_RESTRICTION_KEY)),
      handler(succedd))
  }

  private fun handler(succedd: (LatLng) -> Unit): JsonHttpResponseHandler {
    return object : JsonHttpResponseHandler() {
      override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONObject?) {
        response?.let {
          val location = it.getJSONObject("result").getJSONObject("geometry").getJSONObject("location")
          val lat = location.getDouble("lat")
          val lng = location.getDouble("lng")
          succedd(LatLng(lat, lng))
        }
      }
    }
  }

}