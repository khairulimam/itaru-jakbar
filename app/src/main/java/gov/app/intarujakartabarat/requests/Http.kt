package gov.app.intarujakartabarat.requests

import com.loopj.android.http.AsyncHttpClient

class Http {
  private fun get(): AsyncHttpClient {
    return AsyncHttpClient()
  }

  companion object {
    val client by lazy { Http().get() }
  }
}