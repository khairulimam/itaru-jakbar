package gov.app.intarujakartabarat.utils

import com.google.android.gms.maps.model.LatLng

class TransformableCoordinate(val zone: Tm3Utils.Zone, val coordinate: Pair<Double, Double>) {

  fun getTransformedCoordinate(): LatLng {
    if (isTm3()) return zone.toLatLng(coordinate)
    return LatLng(coordinate.first, coordinate.second)
  }

  private fun isTm3() = coordinate.second !in -180.0..180.0

}