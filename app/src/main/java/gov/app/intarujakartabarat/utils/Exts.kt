package gov.app.intarujakartabarat.utils

import android.location.Location
import android.util.Log
import android.view.View
import com.google.android.gms.maps.model.LatLng
import org.gavaghan.geodesy.Ellipsoid
import org.gavaghan.geodesy.GeodeticCalculator
import org.gavaghan.geodesy.GlobalCoordinates
import org.osgeo.proj4j.BasicCoordinateTransform
import org.osgeo.proj4j.CRSFactory
import org.osgeo.proj4j.ProjCoordinate

fun LatLng.getNewCoordinateWith(bearing: Double, distance: Double): LatLng {
  val calculator = GeodeticCalculator().calculateEndingGlobalCoordinates(
    Ellipsoid.WGS84,
    GlobalCoordinates(latitude, longitude),
    bearing,
    distance
  )
  return LatLng(calculator.latitude, calculator.longitude)
}

fun LatLng.toTm3(): Pair<Double, Double> {
  try {
    val epsgCode = getTm3Zone()?.epsgCode()
    val factory = CRSFactory()
    val srcCrs = factory.createFromName("EPSG:4326")
    val dstCrs = factory.createFromName(epsgCode)
    val transform = BasicCoordinateTransform(srcCrs, dstCrs)
    // Note these are x, y so lng, lat
    val srcCoord = ProjCoordinate(longitude, latitude)
    val dstCoord = ProjCoordinate()
    transform.transform(srcCoord, dstCoord)
    return Pair(dstCoord.x, dstCoord.y)
  } catch (e: IllegalStateException) {
    Log.i("transformation", e.localizedMessage)
  }
  return Pair(0.0, 0.0)
}

fun Tm3Utils.Zone.toLatLng(tm3: Pair<Double, Double>): LatLng {
  try {
    val factory = CRSFactory()
    val dstCrs = factory.createFromName("EPSG:4326")
    val srcCrs = factory.createFromName(epsgCode())
    val transform = BasicCoordinateTransform(srcCrs, dstCrs)
    // Note these are x, y so lng, lat
    val srcCoord = ProjCoordinate(tm3.first, tm3.second)
    val dstCoord = ProjCoordinate()
    transform.transform(srcCoord, dstCoord)
    return LatLng(dstCoord.y, dstCoord.x)
  } catch (e: IllegalStateException) {
    Log.i("transformation", e.localizedMessage)
  }
  return LatLng(0.0, 0.0)
}

fun View.gone() {
  visibility = View.GONE
}

fun View.visible() {
  visibility = View.VISIBLE
}

fun Location.latLng(): LatLng = LatLng(latitude, longitude)

fun Double.format(digits: Int): String = java.lang.String.format("%.${digits}f", this)